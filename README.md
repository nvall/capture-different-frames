## Synopsis

This command gets a video and captures frames between the passed time in seconds and it stores the frames as images into a directory.

## Dependencies

- ffmpeg
- ImageMagick

## Parameters

-h : help <br/>
-v : video file <br/>
-s : number of secords between each captured frame <br/>
-o : the output directory of the stored captured frames <br/>

## Run the Command

> sh capture-diff-frames.sh -v /path/to/video.mp4 -s 30 -o /path/to/output-dir/

## More about the Shell Script

This script uses ffmpeg to extract frames from a video based on a number of seconds. The number of seconds is passed as a paramemter in the command as well as the video file and also the output directory that all the frames get stored as images.<br/><br/>

After exporting all frames, there is a comparison by using the command compare that compares every picture with the next one in order to find the differences. In case that the difference is not more than 10 percent then the currrent picture gets deleted and the next one is kept. The percentage of the different images comparison can be changed inside the script.<br/><br/>

At the end, only the quite different pictures are kept. Also, there is a calculation about the exact video time of each frame and this time is appended at the end of the picture filename. With this way, you can go to the video time frame to see more about the picture frame that has been captured. 

## More about the Creator

Profile: [Nick Vallaris](http://www.nickvallaris.com/about-me)<br/>
Article: [Capturing frames of a video using a shell script](http://www.nickvallaris.com/shell-scripts/capturing-different-frames-of-a-video-using-a-shell-script)