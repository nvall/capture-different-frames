#!/bin/bash

while [ $# -gt 0 ]
do
    case $1 in
        -h | --help)
          echo "$package - attempt to capture frames"
          echo " "
          echo "$package [options] application [arguments]"
          echo " "
          echo "options:"
          echo "-h, --help                show brief help"
          echo "-v, --video FILE          specify the video file"
          echo "-s, --seconds INTEGER     specify the frequency of the seconds between the captured frames"
          echo "-o, --output-dir DIR      specify a directory to store output in"
          exit 0
        ;;
        -v | --video)
          shift
          video=$1
        ;;
        -s | --seconds)
          shift
          secondsPerFrame=$1
        ;;
        -o | --output-dir)
          shift
          outputDir=$1
        ;;
         *)
          echo "No valid option. Type -h or --help for more information"
          exit 0
    esac
    shift
done

# variables for the images comparison
limitDiffPercentage=1000
imgWidth=400
imgHeigh=300
fuzzPercent=10

# colors for the messages
REDC='\033[1;31m'
GREC='\033[1;32m'
YELC='\033[1;33m'
NC='\033[0m'

# parameters validation
if [ -z "$video" ] ;
  then
    echo "${REDC}Error:${NC} No video supplied"
    echo "Type -h or --help for help"
    exit 0
fi

if [ -z "$secondsPerFrame" ] ;
  then
    echo "${REDC}Error:${NC} No seconds supplied"
    echo "Type -h or --help for help"
    exit 0
fi

if [ -z "$outputDir" ] ;
  then
    echo "${REDC}Error:${NC} No store directory supplied"
    echo "Type -h or --help for help"
    exit 0
fi

if [ ! -f "$video" ] ;
    then
    echo "${REDC}Error:${NC} The video: $video does not exist"
    exit 0
fi

if [ ! -d "$outputDir" ] ;
    then
     echo "${REDC}Error:${NC} The output dir: $outputDir does not exist"
     exit 0;
fi

# print information
echo "${YELC}Video:${NC} $video"
echo "${YELC}Frame per seconds:${NC} 1/$secondsPerFrame"
echo "${YELC}Export directory:${NC} $outputDir"

# delete existing images
rm -rf $outputDir/img*

# create tmp file for the progress bar
logfile=$(mktemp)

ffmpeg -loglevel quiet -i $video -vf fps=1/$secondsPerFrame $outputDir/img_%d.jpg && echo 1 > $logfile &

# print dots till ffmpeg finishes
while [ ! -s $logfile ]; do printf "."; sleep 1; done

numberOfFiles=$(find $outputDir/img_* | wc -l)

echo "\n"

for i in `seq 1 $numberOfFiles`
do
  currentFile="${outputDir}/img_${i}.jpg"

  nextImgNum=$(($i+1))

  nextFile="${outputDir}/img_${nextImgNum}.jpg"

  # if the next file exists then there is images comparison
  if [ -f "$nextFile" ] ;
    then
      diffPercentage=$(convert "$currentFile" "$nextFile" -resize "$imgWidth"x"$imgHeigh"\! MIFF:- | compare -metric AE -fuzz "$fuzzPercent%" - null: 2>&1)
      if [ "$diffPercentage" -le "$limitDiffPercentage" ] ;
        then
          rm -rf $currentFile
      fi
  fi

  # if the file still exists then the frame time is being added to the filename
  if [ -f "$currentFile" ] ;
    then
      # total seconds of the frame time
      totalSeconds=$(($i*$secondsPerFrame))

       # calculate hours, minutes and seconds
      hours=$(($totalSeconds/3600))
      minutes=$((($totalSeconds/60)%60))
      seconds=$(($totalSeconds%60)) 

      # add 0 at the beginning if it's single digit
      if [ "$hours" -le 9 ] ;
      then
         hours="0$hours"
      fi

      # add 0 at the beginning if it's single digit
      if [ "$minutes" -le 9 ] ;
      then
         minutes="0$minutes"
      fi

      # add 0 at the beginning if it's single digit
      if [ "$seconds" -le 9 ] ;
      then
         seconds="0$seconds"
      fi

      # add frame time to the filename
      mv $currentFile "$outputDir/img_$i-$hours:$minutes:$seconds.jpg"

      # print file path
      echo "${GREC=}File:${NC} $outputDir/img_$i-$hours:$minutes:$seconds.jpg"
  fi

done
